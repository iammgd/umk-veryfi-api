<?php
    $FILE_NAME = $_GET['file_name'];
    $FILE_URL = $_GET['file_url'];

    //echo $FILE_URL;exit;

    $CLIENT_ID = ""; //insert the client id from verify here..
    $ENVIRONMENT_URL = "api.veryfi.com";

    $username = ""; //insert username from verify here..
    $api_key = ""; //insert api key from verify here..

    # You can send the list of categories that is relevant to your case
    # Veryfi will try to choose the best one that fits this file
    $categories = array("Office Expense", "Meals & Entertainment", "Utilities", "Auto");

    $data = array(
        "file_name" => $FILE_NAME, 
        "file_url" => $FILE_URL,
        "categories" => $categories
    );

    $headers = array(
        "Content-Type: application/json",
        "Accept: application/json",
        "AUTHORIZATION: apikey $username:$api_key",
        "CLIENT-ID: $CLIENT_ID"
    );

    $url = "https://$ENVIRONMENT_URL/api/v7/partner/documents/";

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    $json_response = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    echo($json_response);
?>